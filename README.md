# SimpleSocketServer

#### 项目介绍
简单的基于Socket的TCP服务器，基于.net3.5 ,可以用在任何适用于.net的程序，包括Unity3D。 初始的意图就是用于Unity3D中与外界应用程序通信。

#### 软件架构
软件架构说明
三个项目，第一个SerSockServer，服务端dll库，第二个，控制台程序TestSockServer，用来承载服务器，显示传入的数据。第三个SockClient，客户端。客户端每次调用SRString用来向服务器发送信息并接收服务器端的信息，此为一个会话，接着就关闭连接。下一次再传数据时重新打开连接。


#### 使用说明

可以直接使用源码或使用dll。

服务器端：
```
 var server = SServer.GetServer(        //使用默认端口50100
                x => { s = x; },            //传入到服务器的消息处理函数，x是传入的string。
                () => "Receved:" + s        //从服务器返回消息处理函数，返回值为string类型。
                );

```
客户端：
```
string s = SClient.DownloadString("Hello!");        //向默认服务器localhost,默认端口50100发送数据,并接受返回的数据。

```
