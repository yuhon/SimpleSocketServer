﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleSock;

namespace TestSockServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "";
            var server = SServer.GetServer(
                x => { s = x; },        
                () => "Receved:" + s
                );
            
            Console.WriteLine("Server started!");
            Console.ReadKey();
        }
    }
}
