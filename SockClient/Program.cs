﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using SimpleSock;

namespace SockClient
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                string sinput = Console.ReadLine();
                if (sinput == "q!") break;
                Console.WriteLine(SClient.DownloadString(sinput));
            }
        }
    }
}
