﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace SimpleSock
{
    public class SClient
    {
        /// <summary>
        /// 向服务器发送并接收字符串
        /// </summary>
        /// <param name="s"></param>
        /// <param name="addr">服务器地址，默认localhost</param>
        /// <param name="port">端口,默认50100</param>
        /// <returns>从服务器端返回的字符串,不大于4000B</returns>
        public static string DownloadString(string s, string addr = "localhost", int port = 50100)
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(addr, port);
            socket.Send(Encoding.UTF8.GetBytes(s));
            byte[] buff = new byte[4000];
            int c = socket.Receive(buff);
            socket.Close();
            return Encoding.UTF8.GetString(buff, 0, c);
        }
    }
}
